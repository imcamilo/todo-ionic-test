import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';

@Component({
  selector: 'page-add-item',
  templateUrl: 'add-item.html',
})
export class AddItemPage {

  title: string;
  description: string;
  price: number;

  constructor(public navCtrl: NavController, public navParams: NavParams, public view: ViewController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddItemPage');
  }

  saveItem() {
    console.log('action over saveItem');
    let newItem = {
      title: this.title,
      description: this.description,
      price: this.price
    };
    this.view.dismiss(newItem);
  }

  closeView() {
    console.log('action over closeView');
    this.view.dismiss();
  }
  
  close(){
    this.view.dismiss();
  }

}
