import { Component } from '@angular/core';
import { ModalController, NavController } from 'ionic-angular';
import { AddItemPage } from '../add-item/add-item';
import { ItemDetailPage } from '../item-detail/item-detail';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  public items = [];

  constructor(public navCtrl: NavController, public modalCtrl: ModalController) {

  }

  ionViewDidLoad() { //cuando se carga la pagina

  }
  
  addItem() {
    console.log('add item');
    let addModal = this.modalCtrl.create(AddItemPage); //creo modal y paso el componente
    addModal.onDidDismiss((item) => { //detecta al cerrar modal
      if(item) {
        this.saveItem(item);
      }
    });
    addModal.present(); //abre modal (superpone)
  }

  saveItem(item) {
    console.log('save item');
    this.items.push(item);   
  }

  viewItem(item) { //recibe item, pasa item a item-detail
    console.log('view item');
    this.navCtrl.push(ItemDetailPage, {
      item:item
    })
  }

}
